TARGETS=public/lib/style.css

default: $(TARGETS)
	cd nanoblag && make
	make public/b

public/lib/style.css: src/style.scss
	sassc -t compressed $< > $@ || (rm -f $@; exit 1)

public/b: $(shell find nanoblag/out)
	cp -Tau $< $@

clean:
	rm -f $(TARGETS)
	rm -Rf public/b

host:
	php -S '[::]:8089' -t public

.PHONY: clean default
